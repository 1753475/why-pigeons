@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row pt-5">
        <div class="col-4">
            <img src="https://d1ralsognjng37.cloudfront.net/b53bbb9c-278f-4535-a1c1-8aaa9df1e91c.jpeg" class="w-100"/>
        </div>
        <div class="col-4">
            <img src="https://images.deliveryhero.io/image/fd-ca/LH/s9aa-listing.jpg" class="w-100"/>
        </div>
        <div class="col-4">
            <img src="https://images.deliveryhero.io/image/fd-ca/LH/s9tk-listing.jpg" class="w-100"/>
        </div>
    </div>
</div>
@endsection
