# Food and Maybe some Pigeons?

We will build a restaurant aggregator and delivery agent web application where registered restaurants can list and sell food items from their respective menus and customers can purchase these items to have them delivered to their location. This application targets people that have no time to go and pick up food from a restaurants or do not have the means i.e. a car to go eat out. Customers also include people that want to stay home and eat from the comfort of their home.

As a team, we aim to create a functional application that performs well while having a good U.I. and U.X. for our clientele. We want to design a secure website, use APIs and different software/libraries.


## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
